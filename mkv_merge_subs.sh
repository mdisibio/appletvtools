#!/bin/bash
echo "MKV Sub Merge script starting..."

pushed=false

# Determine working directory
# Input from SABnzbd
#   $1 = Final directory of the job
if [ -n "$1" ] 
then
	if [ -d "$1" ]
	then
		echo "Processing directory specified on command-line: $1"
		pushed=true
		pushd "$1"
	else
		echo "Invalid directory specified on command-line: $1"
		exit
	fi
else
	echo "Processing current directory"
fi

for mkv in ./*.mkv
do
	if [ -f $mkv ] 
	then
		echo "Processing $mkv"
		filename=$(basename "$mkv")
		base="${filename%.*}"

		idx="$base.idx"
		sub="$base.sub"
		out="$base.subbed.mkv"
		
		if [ -f "$idx" ] && [ -f "$sub" ]
		then
			echo " Has .idx and .sub, resubbing"
			
			mkvmerge="/Applications/Mkvtoolnix.app/Contents/MacOS/mkvmerge"
			if [ -x "$mkvmerge" ]
			then
				"$mkvmerge" -o "$out" "$mkv" "$idx"
			else
				echo " ERROR mkvmerge not found at $mkvmerge"
			fi
		else
			echo " Missing .idx or .sub file, nothing to do"
		fi
	fi
done

if $pushed
then popd
fi
echo "script complete."
