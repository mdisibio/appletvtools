

on run
	my mountNetworkFolders()
	
	set libFiles to {}
	set diskFiles to {}
	
	set tracksToDel to {}
	set tracksToAdd to {}
	
	--set source_folder to (alias "raid2:video:Movies:")
	set source_folders to {�
		(alias "raid2:video:"), �
		(alias "raid3:video:Movies"), �
		(alias "raid3:video:TV Shows") �
			}
	--set source_folders to {(alias "raid3:video:Movies")}
	
	
	my logit("----------------------------------------")
	my logit("Scanning iTunes library...")
	my logit("----------------------------------------")
	tell application id "com.apple.iTunes"
		-- go through every track
		set library_playlist to library playlist 1
		set trackCount to count of tracks of library_playlist
		my logit("Scanning " & trackCount & " files in library...")
		
		repeat with track_item in file tracks of library_playlist
			try
				-- if missing then delete
				if location of track_item is missing value then
					set track_name to name of track_item
					set track_id to database ID of track_item
					my logit("Missing library file ID:" & track_id & " Name: " & track_name)
					
					delete track_item
					my logit("...deleted")
				end if
				
				-- good file, add to list
				if not location of track_item is missing value then
					set track_location to location of track_item
					set track_posix to POSIX path of track_location
					-- my logit(track_posix)
					set libFiles to libFiles & track_posix
				end if
			on error number errNum
				my logit("Error: " & errNum)
			end try
		end repeat
	end tell
	
	my logit("...done scanning iTunes. Final good file count: " & (count of libFiles))
	
	set libFiles to quickSort(libFiles)
	
	repeat with sf in source_folders
		my logit("----------------------------------------")
		my logit("Scanning folder: " & sf & " ...")
		my logit("----------------------------------------")
		
		set my diskFiles to my getFolderContents(sf)
		my logit("Found " & (count of diskFiles) & " files")
		
		repeat with df in diskFiles
			set df_path to POSIX path of df
			-- my logit(df_path)
			
			set found to 0
			repeat with lf_path in libFiles
				ignoring case
					if df_path as string is equal to lf_path as string then
						set found to 1
						exit repeat
					end if
				end ignoring
			end repeat
			
			if found is 0 then
				set tracksToAdd to tracksToAdd & df_path
				my logit("Adding new file: " & df_path)
				tell application id "com.apple.iTunes"
					add (df)
				end tell
			end if
		end repeat
		
	end repeat
	
	log "----------------------------------------"
	log "      SCRIPT COMPLETE"
	log "----------------------------------------"
	
end run

to logit(log_string)
	log log_string
end logit

on getFolderContents(mSource_folder)
	set output to {}
	set item_list to ""
	
	-- get contents
	tell application "System Events"
		set item_list to get the path of every disk item of mSource_folder
	end tell
	
	set item_count to (get count of items in item_list)
	repeat with i from 1 to item_count
		
		-- get current file properties
		set the_item to item i of the item_list
		tell current application
			set file_info to get info for alias the_item
		end tell
		
		if visible of file_info is true then
			-- file - add to output
			if folder of file_info is false then
				set end of output to the_item
			end if
			
			-- subfolder - recursively scan
			if folder of file_info is true then
				set output to output & my getFolderContents(alias the_item)
			end if
		end if
		
	end repeat
	
	return output
end getFolderContents

on quickSort(theList)
	--public routine, called from your script
	script bs
		property alist : theList
		
		on Qsort(leftIndex, rightIndex)
			--private routine called by quickSort. 
			--do not call from your script!
			if rightIndex > leftIndex then
				set pivot to ((rightIndex - leftIndex) div 2) + leftIndex
				set newPivot to Qpartition(leftIndex, rightIndex, pivot)
				set theList to Qsort(leftIndex, newPivot - 1)
				set theList to Qsort(newPivot + 1, rightIndex)
			end if
			
		end Qsort
		
		on Qpartition(leftIndex, rightIndex, pivot)
			--private routine called by quickSort. 
			--do not call from your script!
			set pivotValue to item pivot of bs's alist
			set temp to item pivot of bs's alist
			set item pivot of bs's alist to item rightIndex of bs's alist
			set item rightIndex of bs's alist to temp
			set tempIndex to leftIndex
			repeat with pointer from leftIndex to (rightIndex - 1)
				if item pointer of bs's alist � pivotValue then
					set temp to item pointer of bs's alist
					set item pointer of bs's alist to item tempIndex of bs's alist
					set item tempIndex of bs's alist to temp
					set tempIndex to tempIndex + 1
				end if
			end repeat
			set temp to item rightIndex of bs's alist
			set item rightIndex of bs's alist to item tempIndex of bs's alist
			set item tempIndex of bs's alist to temp
			
			return tempIndex
		end Qpartition
		
	end script
	
	if length of bs's alist > 1 then bs's Qsort(1, length of bs's alist)
	return bs's alist
end quickSort

on mountNetworkFolders()
	
	mount volume "cifs://server2.local/raid2"
	mount volume "cifs://server2.local/raid3"
	
end mountNetworkFolders
